const express = require('express');
import { ApolloServer } from 'apollo-server-express';
import schema from './data/resolvers'

import dotenv from 'dotenv'
dotenv.config();


const app = express();
const port = process.env.PORT || 5000;

console.log(port)


const server = new ApolloServer({
  schema,
  // secret: process.env.SECRET,
  introspection: true,
  playground: true,

  engine: process.env.ENGINE_API_KEY && {
    apiKey: process.env.ENGINE_API_KEY,
  },
});
server.applyMiddleware({ app });
/*
app.use('/graphql', graphqlhttp({
  schema,
  graphiql: true
}));*/

app.listen(port, () => {
  console.log(`Express server listening on port ${server.graphqlPath}`);
});

/*
curl -X POST -H "Content-Type:application/json" \
-d '{"query": "{getPersona(id: \"1\"){nombre}}"}' \
http://localhost:3000/graphql


*/