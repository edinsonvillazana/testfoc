import dotenv from 'dotenv'
dotenv.config();

const initOptions = {
    // global event notification;
    error(error, e) {
        if (e.cn) {
            // A connection-related error;
            //
            // Connections are reported back with the password hashed,
            // for safe errors logging, without exposing passwords.
            console.log('CN:', e.cn);
            console.log('EVENT:', error.message || error);
        }
    }
};



const pgp = require('pg-promise')({
    noWarnings: true
});
{
    noWarnings: true
}

// using an invalid connection string:
const config = {

    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT
};

const psql = pgp(config);

psql.connect()
    .then(obj => {
        obj.done(); // success, release the connection;
    })
    .catch(error => {
        console.log('ERROR:', error.message || error);
    });

export { psql }

/*
import dotenv from 'dotenv'
dotenv.config();
// Database connection parameters:

const config = {

   user: process.env.DB_USER,
   host: process.env.DB_HOST,
   database: process.env.DB_DATABASE,
   password: process.env.DB_PASS,
   port: process.env.DB_PORT,
};

const pgp = require('pg-promise')();
const psql = pgp(config);
export { psql }*/




