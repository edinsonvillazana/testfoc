import {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLID,
  GraphQLBoolean,
  GraphQLNonNull
} from 'graphql/type';

import {
  SpacesInfo,
  PersonType,
  UserInputType,
  MaterialType,
  MaterialInputType,
  EquipmentsType,
  EquipmentsInputType,
  SpacesType,
  SpacesInputType,
  OptionType,
  OptionType2,
  CareerType,
  ActivityType,
  ActivityInputType,
  UserLoginType,
  UserLoginInputType,
  TokenType,
  FacilitatorType,
  FacilitatorInputType,
  SemesterType
} from "./usersTypes";

import { psql } from "../connection"
import dotenv from "dotenv";
dotenv.config();

import { tokenForUser } from "../authentication";

const userQueries = {
  getPersona: {
    type: FacilitatorType,
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      const query = `SELECT * FROM facilitador WHERE id = $1`;
      const values = [args.id];
      return psql
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },

  getMaterial: {
    type: MaterialType,
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      const query = `SELECT * FROM materiales WHERE id = $1`;
      const values = [args.id];
      return psql
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },
  allMaterials: {
    type: GraphQLList(MaterialType),
    args: {
      limit: { type: GraphQLInt },
      offset: { type: GraphQLInt }
    },
    resolve(parent, args) {
      const values = [args.limit, args.offset];
      const query = `SELECT * FROM materiales ORDER BY nombre ASC limit $1 offset $2`;
      return psql.any(query, values);
    }
  },
  allEquipments: {
    type: GraphQLList(EquipmentsType),
    args: {
      limit: { type: GraphQLInt },
      offset: { type: GraphQLInt }
    },
    resolve(parent, args) {
      const values = [args.limit, args.offset];
      const query = `SELECT * FROM equipos ORDER BY nombre ASC limit $1 offset $2`;
      return psql.any(query, values);
    }
  },
  getEquipment: {
    type: EquipmentsType,
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      const query = `SELECT * FROM equipos WHERE id = $1`;
      const values = [args.id];
      return psql
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },
  getSpace: {
    type: SpacesType,
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      const query = `SELECT * FROM espacios WHERE id = $1`;
      const values = [args.id];
      return psql
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },

  getActivity: {
    type: SpacesType,
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      const query = `SELECT * FROM public.actividades WHERE id = $1`;
      const values = [args.id];
      return psql
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },

  allSpaces: {
    type: GraphQLList(SpacesType),
    args: {
      limit: { type: GraphQLInt },
      offset: { type: GraphQLInt }
    },
    resolve(parent, args) {
      const values = [args.limit, args.offset];
      const query = `SELECT * FROM espacios ORDER BY nombre ASC limit $1 offset $2`;
      return psql.any(query, values);
    }
  },
  allActivity: {
    type: GraphQLList(ActivityType),
    args: {
      limit: { type: GraphQLInt },
      offset: { type: GraphQLInt }
    },
    resolve(parent, args) {
      const values = [args.limit, args.offset];
      const query = `SELECT actividades.id,
        actividades.status, actividades.nombre, actividades.cupo, actividades.hora, actividades.semestre,
        facilitador.nombre AS facilitador,
        semestre.semestre AS semestre,
        carrera.carrera AS carrera from public.actividades
        JOIN facilitador ON (actividades.facilitador = facilitador.id)
        JOIN carrera ON (actividades.carrera = carrera.id)
        JOIN semestre ON (actividades.semestre = semestre.id)
        ORDER BY nombre ASC limit $1 offset $2`;
      return psql.any(query, values);
    }
  },

  countActivity: {
    type: GraphQLInt,
    resolve: function resolve() {
      return psql.one("SELECT count(id) FROM public.actividades;", [], function (
        c
      ) {
        return +c.count;
      });
    }
  },

  countEquipment: {
    type: GraphQLInt,
    resolve: function resolve() {
      return psql.one("SELECT count(id) FROM public.equipos;", [], function (c) {
        return +c.count;
      });
    }
  },

  countMaterial: {
    type: GraphQLInt,
    resolve: function resolve() {
      return psql.one("SELECT count(id) FROM public.materiales;", [], function (
        c
      ) {
        return +c.count;
      });
    }
  },

  countSpaces: {
    type: GraphQLInt,
    resolve() {
      return psql.one(
        "SELECT count(id) FROM public.espacios;",
        [],
        c => +c.count
      );
    }
  },
  allOptions: {
    type: GraphQLList(OptionType),
    args: {
      table: { type: GraphQLString }
    },
    resolve(parent, args) {
      const table = "tipo_" + args.table;
      return psql.query("SELECT  $1:name FROM $2:name", ["*", table]);
    }
  },

  allCareer: {
    type: GraphQLList(CareerType),
    resolve() {
      return psql.any("SELECT * FROM carrera ORDER BY carrera ASC");
    }
  },

  allFacilitator: {
    type: GraphQLList(FacilitatorType),
    resolve() {
      return psql.any("SELECT * FROM facilitador ORDER BY nombre ASC");
    }
  },

  getFacilitator: {
    type: FacilitatorType,
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      const query = `SELECT * FROM facilitador WHERE id = $1`;
      const values = [args.id];
      return psql
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },

  allSemester: {
    type: GraphQLList(SemesterType),
    resolve() {
      return psql.any("SELECT * FROM semestre ORDER BY semestre ASC");
    }
  },

  searchOptions: {
    type: GraphQLList(OptionType2),
    args: {
      table: { type: GraphQLString }
    },
    resolve(parent, args) {
      const table = "tipo_" + args.table;
      return psql.task(t => {
        return t
          .oneOrNone(
            "SELECT id FROM select_options WHERE nombre like '$1:value'",
            table,
            u => u && u.id
          )
          .then(userId => {
            return t.any(
              "SELECT * FROM select_options where table_id=$1",
              userId
            );
          });
      });
    }
  }
};

const userMutations = {
  addPerson: {
    type: new GraphQLList(PersonType),
    args: {
      input: {
        type: UserInputType
      }
    },
    resolve(parent, args) {
      return psql.any(
        ` INSERT INTO personas(
                    nombre, cedula, apellido, sexo_id)
           VALUES ($1, $2, $3, $4) RETURNING *`,
        [
          args.input.nombre,
          args.input.cedula,
          args.input.apellido,
          args.input.sexo_id
        ]
      );
    }
  },


  addMaterial: {
    type: new GraphQLList(MaterialType),
    args: {
      input: {
        type: MaterialInputType
      }
    },
    resolve(parent, args) {
      console.log(args);
      return psql.any(
        ` INSERT INTO public.materiales(
                    nombre, status, cantidad)
            VALUES ( $1, $2, $3) RETURNING *`,
        [args.input.nombre, args.input.status, args.input.cantidad]
      );
    }
  },
  addActivity: {
    type: new GraphQLList(ActivityType),
    args: {
      input: {
        type: ActivityInputType
      }
    },
    resolve(parent, args) {
      return psql.any(
        ` INSERT INTO public.actividades(
                    nombre, status, facilitador, cupo, hora, carrera, semestre)
            VALUES ( $1, $2, $3, $4, $5, $6, $7) RETURNING *`,
        [
          args.input.nombre,
          args.input.status,
          args.input.facilitador,
          args.input.cupo,
          args.input.hora,
          args.input.carrera,
          args.input.semestre
        ]
      );
    }
  },
  addEquipment: {
    type: new GraphQLList(EquipmentsType),
    args: {
      input: {
        type: EquipmentsInputType
      }
    },
    resolve(parent, args) {
      return psql.any(
        ` INSERT INTO public.equipos(nombre, status, cantidad)
            VALUES ( $1, $2, $3) RETURNING *`,
        [args.input.nombre, args.input.status, args.input.cantidad]
      );
    }
  },
  updateEquipment: {
    type: new GraphQLList(EquipmentsType),
    args: {
      id: { type: GraphQLInt },
      input: {
        type: EquipmentsInputType
      }
    },
    resolve(parent, args) {
      return psql.any(
        `UPDATE public.equipos
                    SET nombre=$1, status=$2, cantidad=$3
                WHERE id=${args.id} RETURNING *`,
        [args.input.nombre, args.input.status, args.input.cantidad]
      );
    }
  },
  updateSpace: {
    type: new GraphQLList(SpacesType),
    args: {
      id: { type: GraphQLInt },
      input: {
        type: SpacesInputType
      }
    },
    resolve(parent, args) {
      return psql.any(
        `UPDATE public.espacios
                    SET nombre=$1, status=$2, codigo=$3
                WHERE id=${args.id} RETURNING *`,
        [args.input.nombre, args.input.status, args.input.codigo]
      );
    }
  },

  updateMaterial: {
    type: new GraphQLList(MaterialType),
    args: {
      id: { type: GraphQLInt },
      input: { type: MaterialInputType }
    },
    resolve(parent, args) {
      console.log(args);
      return psql.any(
        `UPDATE public.materiales
                    SET nombre=$1, status=$2, cantidad=$3
                WHERE id=${args.id} RETURNING *`,
        [args.input.nombre, args.input.status, args.input.cantidad]
      );
    }
  },

  deleteSpace: {
    type: new GraphQLList(SpacesType),
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      return psql
        .result("DELETE FROM espacios WHERE id = $1", [args.id])
        .then(result => {
          console.log(result.rowCount);
        })
        .catch(error => {
          console.log("ERROR:", error);
        });
    }
  },
  deletePeople: {
    type: new GraphQLList(PersonType),
    args: { id: { type: GraphQLString } },
    resolve(parent, args) {
      console.log(args.id, parent);
      return psql
        .result("DELETE FROM people WHERE id = $1", [args.id])
        .then(result => {
          console.log(result.rowCount);
        })
        .catch(error => {
          console.log("ERROR:", error);
        });
    }
  },

  deleteEquipment: {
    type: new GraphQLList(EquipmentsType),
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      return psql
        .result("DELETE FROM equipos WHERE id = $1", [args.id])
        .then(result => {
          console.log(result.rowCount);
        })
        .catch(error => {
          console.log("ERROR:", error);
        });
    }
  },

  deleteMaterial: {
    type: new GraphQLList(MaterialType),
    args: { id: { type: GraphQLInt } },
    resolve(parent, args) {
      return psql
        .result("DELETE FROM materiales WHERE id = $1", [args.id])
        .then(result => {
          console.log(result.rowCount);
        })
        .catch(error => {
          console.log("ERROR:", error);
        });
    }
  }
};

export { userQueries, userMutations };
