import {
  GraphQLString,
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLList,
  GraphQLInt
} from "graphql";

const UserLoginType = new GraphQLObjectType({
  name: "userLogin",
  fields: () => ({
    usuario: { type: GraphQLString },
    contrasena: { type: GraphQLString }
  })
});

const UserLoginInputType = new GraphQLInputObjectType({
  name: "userLoginInputType",
  fields: () => ({
    usuario: { type: GraphQLString },
    contrasena: { type: GraphQLString }
  })
});

const TokenType = new GraphQLObjectType({
  name: "Token",
  fields: () => ({
    token: { type: GraphQLString }
  })
});

const UserInputType = new GraphQLInputObjectType({
  name: "UserInputType",
  fields: () => ({
    nombre: { type: new GraphQLNonNull(GraphQLString) },
    apellido: { type: new GraphQLNonNull(GraphQLString) },
    cedula: { type: new GraphQLNonNull(GraphQLString) },
    sexo_id: { type: GraphQLInt }
  })
});

const PersonType = new GraphQLObjectType({
  name: "Persona",
  fields: () => ({
    id: { type: GraphQLString },
    nombre: { type: GraphQLString },
    apellido: { type: GraphQLString },
    cedula: { type: GraphQLString },
    sexo_id: { type: GraphQLInt }
  })
});

const FacilitatorInputType = new GraphQLObjectType({
  name: "FacilitatorInputType",
  fields: () => ({
    nombre: { type: GraphQLString },
    apellido: { type: GraphQLString },
    cedula: { type: GraphQLString },
    especialidad: { type: GraphQLString },
    sexo_id: { type: GraphQLInt }
  })
});

const FacilitatorType = new GraphQLObjectType({
  name: "Facilitator",
  fields: () => ({
    id: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    apellido: { type: GraphQLString },
    cedula: { type: GraphQLString },
    especialidad: { type: GraphQLString },
    sexo_id: { type: GraphQLInt }
  })
});

const SemesterType = new GraphQLObjectType({
  name: "Semester",
  fields: () => ({
    id: { type: GraphQLInt },
    semestre: { type: GraphQLString }
  })
});

const MaterialType = new GraphQLObjectType({
  name: "Material",
  fields: () => ({
    id: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt },
    cantidad: { type: GraphQLInt }
  })
});

const MaterialInputType = new GraphQLInputObjectType({
  name: "MaterialInputType",
  fields: () => ({
    nombre: { type: new GraphQLNonNull(GraphQLString) },
    status: { type: new GraphQLNonNull(GraphQLInt) },
    cantidad: { type: new GraphQLNonNull(GraphQLInt) }
  })
});

const EquipmentsType = new GraphQLObjectType({
  name: "EquipmentsType",
  fields: () => ({
    id: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt },
    cantidad: { type: GraphQLInt }
  })
});

const EquipmentsInputType = new GraphQLInputObjectType({
  name: "EquipmentsInputType",
  fields: () => ({
    nombre: { type: new GraphQLNonNull(GraphQLString) },
    status: { type: new GraphQLNonNull(GraphQLInt) },
    cantidad: { type: new GraphQLNonNull(GraphQLInt) }
  })
});

const SpacesType = new GraphQLObjectType({
  name: "Spaces",
  fields: () => ({
    id: { type: GraphQLInt },
    offset: { type: GraphQLInt },
    limit: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt },
    codigo: { type: GraphQLString }
  })
});

const SpacesInputType = new GraphQLInputObjectType({
  name: "SpacesInputType",
  fields: () => ({
    nombre: { type: new GraphQLNonNull(GraphQLString) },
    status: { type: new GraphQLNonNull(GraphQLInt) },
    codigo: { type: new GraphQLNonNull(GraphQLString) }
  })
});

const ActivityType = new GraphQLObjectType({
  name: "ActivityType",
  fields: () => ({
    id: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt },
    facilitador: { type: GraphQLString },
    cupo: { type: GraphQLInt },
    hora: { type: GraphQLInt },
    carrera: { type: GraphQLString },
    semestre: { type: GraphQLString }
  })
});

const ActivityInputType = new GraphQLInputObjectType({
  name: "ActivityInputType",
  fields: () => ({
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt },
    facilitador: { type: GraphQLInt },
    cupo: { type: GraphQLInt },
    hora: { type: GraphQLInt },
    carrera: { type: GraphQLInt },
    semestre: { type: GraphQLInt }
  })
});

const SpacesInfo = new GraphQLObjectType({
  name: "SpacesInfo",
  fields: {
    count: { type: GraphQLString }
  }
});

const OptionType = new GraphQLObjectType({
  name: "Option",
  fields: () => ({
    id: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt }
  })
});

const OptionType2 = new GraphQLObjectType({
  name: "Option_select",
  fields: () => ({
    id: { type: GraphQLInt },
    tablet_id: { type: GraphQLInt },
    nombre: { type: GraphQLString },
    status: { type: GraphQLInt },
    tipo_options: { type: GraphQLInt }
  })
});

const CareerType = new GraphQLObjectType({
  name: "Career",
  fields: () => ({
    id: { type: GraphQLInt },
    carrera: { type: GraphQLString }
  })
});

export {
  SpacesInfo,
  PersonType,
  UserInputType,
  MaterialType,
  MaterialInputType,
  EquipmentsType,
  EquipmentsInputType,
  SpacesType,
  SpacesInputType,
  OptionType,
  OptionType2,
  CareerType,
  ActivityType,
  ActivityInputType,
  UserLoginInputType,
  UserLoginType,
  TokenType,
  FacilitatorType,
  FacilitatorInputType,
  SemesterType
};
