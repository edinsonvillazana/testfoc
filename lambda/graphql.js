import { ApolloServer, gql } from "apollo-server-lambda";


const typeDefs = gql`
  type Query {
    hello: String
  }
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    hello: () => 'Hello world!',
  },
};


const server = new ApolloServer({
  introspection: true,
  playground: true,
  typeDefs,
  resolvers,
  cors: true,
});

const handler = server.createHandler();

export { handler }