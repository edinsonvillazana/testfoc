import { ApolloServer, gql } from "apollo-server-lambda";
import schema from './data/resolvers'

const typeDefs = gql`
  type Query {
    hello: String
  }
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    hello: () => 'Hello world!',
  },
};


const server = new ApolloServer({
  introspection: true,
  playground: true,
  schema,
  cors: true,
});

const handler = server.createHandler();

export { handler }