import dotenv from 'dotenv'
dotenv.config();

// using an invalid connection string:

const pgp = require('pg-promise')();

let instance = null;

module.exports = class Connector {

    constructor(user, psw) {

        if (!instance || (user && psw)) {
            instance = this;

            this.config = {
                user: process.env.DB_USER,
                host: process.env.DB_HOST,
                database: process.env.DB_DATABASE,
                password: process.env.DB_PASS,
                port: process.env.DB_PORT
            };

            this.db = pgp(this.config);
        }

        return instance;
    }

}


/*
import dotenv from 'dotenv'
dotenv.config();
// Database connection parameters:

const config = {

   user: process.env.DB_USER,
   host: process.env.DB_HOST,
   database: process.env.DB_DATABASE,
   password: process.env.DB_PASS,
   port: process.env.DB_PORT,
};

const pgp = require('pg-promise')();
const psql = pgp(config);
export { psql }*/




