
import dotenv from 'dotenv'

dotenv.config();
//import jwt from 'jwt-simple'


import jwt from 'jsonwebtoken'

const tokenForUser = (user) => {

    return jwt.sign({ userId: user.id }, process.env.SECRET);
    /* const timestamp = new Date().getTime()
     return jwt.encode({ sub: user.id, iat: timestamp }, process.env.SECRET)*/
}

export { tokenForUser }



