const pgPromise = require('pg-promise');

const pgp = pgPromise({});
import dotenv from 'dotenv'
dotenv.config();

console.log(process.env.DB_USER, process.env.DB_HOST, process.env.DB_DATABASE, process.env.DB_PASS, process.env.DB_PORT
)

const pool = {

    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    ssl: true,
    sslfactory: 'org.postgresql.ssl.NonValidatingFactory'

};

const psql = pgp(pool);


export { psql }

